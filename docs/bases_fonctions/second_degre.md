---
author: Mireille Coilhac
title: Le second degré
tags:
    - fonctions
---

!!! abstract "Le second degré"

    Une série de vidéos pour comprendre l'essentiel du cours sur le second degré par Jean-Yves LABOUCHE

!!! success "Le cours en vidéos"

    |La méthode de résolution d'une équation du second degré.|Déterminer le signe d'un polynôme du second degré<br> (sous sa forme développée)|
    | :------------: | :-------------: | 
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/KRlOrbevLq8?si=Zw7n4CPqFtTNcyHD" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/ozqxUmZ7HL0?si=JLvINGwY0yTLh6XP" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|

  
    |Déterminer le signe d'un polynôme du second degré<br> sous sa forme factorisée|Factoriser un polynôme du second degré<br> en utilisant les formules|
    | :------------: | :-------------: | 
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/ozqxUmZ7HL0?si=JLvINGwY0yTLh6XP" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/gi7DW5NEWA8?si=uLG_UgNay7QtKqu0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|

!!! abstract "À vous de jouer"

    Transcrivez sur papier, sous forme de cartes recto/verso questions/réponses les différentes formules et méthodes vues dans ces vidéos

    