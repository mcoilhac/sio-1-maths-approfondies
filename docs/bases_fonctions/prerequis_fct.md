---
author: Mireille Coilhac
title: Les fonctions
tags:
    - fonctions
---

## Généralités sur les fonctions

[Généralités sur les fonctions - Prérequis](a_telecharger/generalites_fonctions.pdf){ .md-button target="_blank" rel="noopener" }

[Généralités sur les fonctions - 1](https://coopmaths.fr/alea/?uuid=77d18&id=3F1-act&n=10&d=10&s=2&alea=lVf9&cd=1&uuid=0eecd&id=3F10-1&n=3&d=10&s=3&alea=3swO&cd=1&uuid=c9382&id=3F10-5&n=8&d=10&s=3&s2=3&s3=5&alea=tl27&cd=1&uuid=ba520&id=3F10-2&n=6&d=10&s=5&s2=3&s3=5&alea=a99x&cd=1&uuid=4daef&id=3F10-3&n=10&d=10&s=5&alea=4DjD&cd=1&uuid=8a78e&id=3F10-6&alea=ogZQ&uuid=28997&id=2F20-4&n=1&d=10&s=3&s2=8&s3=false&alea=uBOS&cd=1&uuid=277d3&id=2F12-2&n=3&d=10&s=4&alea=9SJJ&cd=1&v=eleve&es=0111001&title=){ .md-button target="_blank" rel="noopener" }

[Généralités sur les fonctions - 2](a_telecharger/generalites_fcts_sujet.pdf){ .md-button target="_blank" rel="noopener" }

[Correction : Généralités sur les fonctions - 2](a_telecharger/generalites_fonctions_correction.pdf){ .md-button target="_blank" rel="noopener" }





## La dérivation

[Skieur géogébra](https://www.geogebra.org/m/wemqzb3y){ .md-button target="_blank" rel="noopener" }

[Dérivation - la base](a_telecharger/Derivation.pdf){ .md-button target="_blank" rel="noopener" }

!!! info "Variations d'une fonction : à retenir"

    Dans cette partie, $f$ est une fonction numérique définie sur un intervalle $I$, $\mathcal{C}$ sa courbe représentative dans un repère. 

    L'idée est qu'il y a un lien entre le signe du coefficient directeur de la tangente de la courbe $\mathcal{C}$ et le sens de variation de la fonction $f$.

    ???+ question "Compléter"

        * $f$ est croissante sur $I \Longleftrightarrow$ ...
        * $f$ est décroissante sur $I \Longleftrightarrow$ ...
        * $f$ est constante sur $I \Longleftrightarrow$ ...

        ??? success "À retenir"

            * $f$ est croissante sur $I \Longleftrightarrow f'(x)\geqslant0$ pour tout $x\in I$.
            * $f$ est décroissante sur $I \Longleftrightarrow f'(x)\leqslant0$ pour tout $x\in I$.
            * $f$ est constante sur $I \Longleftrightarrow f'(x)=0$ pour tout $x\in I$.

!!! example "Exemple d'une fonction polynôme"

    Soit $f$ la fonction définie et dérivable sur $\mathbb{R}$ par $f(x)=2x^3-3x^2-12x-1$. 
    
    Déterminons son sens de variation :

    * Pour tout réel $x$ on a $f'(x)=6x^2-6x-12=6(x^2-x-2)$.
       
    * On détermine le signe de$f'(x)$, et on en déduit le tableau de variations de la fonction $f$ :

    $f'(x)=6(x+1)(x-2)$ 

    ![nom image](images/tab_var_polynome.png ){ width=35% }

    On en déduit les sens de variations d $f$ : 
    
    * $f$ est croissante sur $]-\infty~;-1~]$ 
    * $f$ est décroissante sur $[~-1~;~2~]$
    * $f$ est croissante sur $[~2~;+\infty~[$



[Exercices : formules de dérivation](https://coopmaths.fr/alea/?uuid=ec088&id=1AN14-3&n=5&d=10&s=6&s2=true&s3=false&alea=wu0A&cd=1&uuid=a83c0&id=1AN14-4&n=6&d=10&s=5&alea=UCY4&cd=1&uuid=1a60f&id=1AN14-5&n=5&d=10&s=6&alea=BUiw&cd=1&uuid=b32f2&id=1AN14-6&alea=naqO&uuid=3391d&id=1AN14-7&alea=wckV&v=eleve&es=0111001&title=){ .md-button target="_blank" rel="noopener" }

[Exercices : Dérivation : exercices de base](a_telecharger/Derivation_exos_revision_rectif.pdf){ .md-button target="_blank" rel="noopener" }

Attention : Exercice 2 question 1.b. Il y a une erreur. Il faut lire (il manque un signe "-") : 

$$
g'(x)=3x^2-\dfrac{-2}{x^3}
$$  

[Correction : Dérivation : exercices de base](a_telecharger/derivation_exos_revision_corr.pdf){ .md-button target="_blank" rel="noopener" }


## Etude d'une fonction polynôme du troisième degré

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/qrj3JzAriw8?si=JpahMOcHJnmKIZ_X" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


[Etude de fonctions polynômes du 3ème degré](https://coopmaths.fr/alea/?uuid=e1890&id=1AN20-4&n=5&d=10&s=4&cd=1&v=eleve&es=0211001&title=){ .md-button target="_blank" rel="noopener" }


## Etude d'une fonction polynôme de degré 4

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/6IVxQ6piC1c?si=LbGanWHdStc7yQEr" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


## Cours complet sur les études de fonctions

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2Owr18MregQ?si=Oyrtkeb2WUVYZAqW" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## Exercices variés

[Exercices - série 1](https://coopmaths.fr/alea/?uuid=zert&id=TSA2-QCM07&uuid=c7f8e&id=can1F19&n=5&d=10&cd=1&v=eleve&es=0111001&title=){ .md-button target="_blank" rel="noopener" }

[Exercices - série 2](https://coopmaths.fr/alea/?uuid=0e984&id=can1F15&alea=tmpN&uuid=ad915&id=can1F23&alea=iets&uuid=e3c_2021_06_ameriquenord_3&alea=RqXb&uuid=e3c_2021_01_specimen1_3&alea=qIUZ&uuid=e3c_2020_00_sujet65_2&alea=WKxQ&uuid=e3c_2020_00_sujet58_3&alea=aWXr&uuid=e3c_2020_00_sujet57_2&alea=GpWA&uuid=e3c_2020_00_sujet50_2&alea=ZKsC&uuid=e3c_2020_00_sujet47_3&alea=JTvG&uuid=e3c_2020_00_sujet46_4&alea=WnR8&uuid=e3c_2020_00_sujet45_4&alea=ffgL&uuid=e3c_2020_00_sujet44_2&alea=KnAg&uuid=e3c_2020_00_sujet41_4&alea=Da6r&uuid=e3c_2020_00_sujet37_4&alea=d6lR&uuid=e3c_2020_00_sujet36_4&alea=TkzM&uuid=e3c_2020_00_sujet34_4&alea=zgsL&uuid=e3c_2020_00_sujet27_2&alea=2YkA&uuid=e3c_2020_00_sujet26_2&alea=QmXL&uuid=e3c_2020_00_sujet20_2&alea=QFLl&uuid=e3c_2020_00_sujet19_3&alea=THio&v=eleve&es=0111001&title=){ .md-button target="_blank" rel="noopener" }

## Crédits

D'après des documents de Cédric Pierquet et de Nathalie Daval