---
author: Mireille Coilhac
title: 🏡 Accueil
---
  

😊  Bienvenue dans la classe de mathématiques approfondies de SIO 1 !

!!! info "S'entraîner à l'infini"

    Dans certaines feuilles d'exercices, on peut cliquer sur "Nouvel énoncé". De nouveaux exercices, avec leur correction à la demande, sont alors proposés.
    Vous pouvez donc vous entraîner à l'infini 😉 ...

Ce site évoluera pendant l'année




