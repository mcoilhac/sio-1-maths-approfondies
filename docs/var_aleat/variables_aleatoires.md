---
author: Mireille Coilhac
title: Variables aléatoires discrètes
tags:
    - probabilités
---

## I. Introduction

???+ question "Un jeu d'argent"

    ![intro urnes](images/intro_urnes.png){ width=20% }  

    Une expérience aléatoire se déroule en deux temps :

    On tire, au hasard, une boule dans un sac contenant trois boules rouges numérotées 0, 1, 2 ;  
    puis on tire, au hasard, une boule dans un autre sac contenant quatre boules noires numérotées 1, 2, 3, 4.

    Les **issues** de cette **expérience aléatoire** sont donc des couples de nombres.  
    Les tirages se font au hasard, chaque issue est **équiprobable**.

    **1.** À l’aide d’un arbre, déterminer tous les couples qui peuvent être obtenus.

    ??? success "Indice 1"

        ![intro urnes](images/intro_arbre_1.png){ width=15%}

        <!--
        graph LR
        O( )
        A( )
        B( )
        C( )
        D( )
        E( )
        F( )
        G( )
        H( )
        I( )
        J( )
        K( )
        M( )
        N( )
        P( )
        Q( )
        O --- A 
        O --- B
        O --- C
        A --- D
        A --- E
        A --- F
        A --- G
        B --- H
        B --- I
        B --- J
        B --- K
        C --- M 
        C --- N
        C --- P
        C --- Q
        -->

    ??? success "Réponse"

        ![arbre 1](images/intro_arbre_2.png){ width=10%}

        $$
        E={(0; 1), (0; 2), (0; 3), (0; 4), (1; 1), (1; 2), (1; 3), (1; 4), (2; 1), (2; 2), (2; 3), (2; 4)}
        $$

    **2.** On s’intéresse au produit des nombres de chaque couple. On note $X$ le résultat obtenu. Déterminer tous les produits possibles et les noter au bout des branches de l’arbre.

    ??? success "Réponse"

        ![arbre 1](images/intro_arbre_3.png){ width=18%}

    **3.** Les tirages sont maintenant utilisés pour un jeu : pour une mise de 1 €, on gagne 4 € si le produit est impair, sinon on ne gagne rien. Dans les deux cas, on perd la mise de 1 €. On note $Y$ le gain (qui peut être négatif) obtenu à chaque fois. Déterminer tous les gains possibles et les noter au bout des branches de l’arbre.

    ??? success "Réponse"

        ![arbre 1](images/intro_arbre_4.png){ width=20%}

    !!! info "Variable aléatoire"

        Dans cet exemple on, a défini deux variables aléatoires $X$ et $Y$

        $X$ peut prendre comme valeurs : 0; 1; 2; 3; 4; 6; 8.  
        $Y$ peut prendre comme valeurs : 3; -1.

## II. Variables aléatoires

!!! info "Définition"

    * On dit qu'on associe une variable aléatoire sur un ensemble $E$ lorsqu'à chaque issue de $E$ on associe un nombre réel.
    * Une variable aléatoire est donc une fonction de l'univers $E$ vers l'ensemble $\mathbb{R}$

### loi de probabilité d'une variable aléatoire

Reprenons l'exemple de l'introduction.

!!! info "Notation"

    La probabité que $X$ soit égal à 0 est d'après l'arbre $\dfrac{4}{12}$

    On note : $P(X=0)=\dfrac{4}{12}$

!!! info "Loi de probabilité"

    La loi de probabilité est donnée par le tableau suivant :

    |$x_i$|0|1|2|3|4|6|8|
    | :---    | :----: | :----: |:----: |:----: |:----: |:----: |:----: |
    | $P(X=x_i)$|   |   |   |   |   |   | |

    👉 Il faut écrire les valeurs de $x_i$ dans l'ordre croissant dans la première ligne du tableau

???+ question "Le jeu d'argent"

    **1.** Donner la loi de probabilité de la variable aléatoire $X$

    ??? success "Solution"

        |$x_i$|0|1|2|3|4|6|8|
        | :---    | :----: | :----: |:----: |:----: |:----: |:----: |:----: |
        | $P(X=x_i)$| $\dfrac{4}{12}$  |  $\dfrac{1}{12}$  |  $\dfrac{2}{12}$  | $\dfrac{1}{12}$   | $\dfrac{2}{12}$   | $\dfrac{1}{12}$   |$\dfrac{1}{12}$ |

    👉 Il n'est pas nécessaire de simplifier les fractions obtenues. Cela permet très rapidement de vérifier que la somme de toutes les probabilités obtenues est bien égale à 1.

    **2.** Donner la loi de probabilité de la variable aléatoire $Y$

    ??? success "Solution"

        |$y_i$|-1|3|
        | :---    | :----: | :----: | 
        | $P(Y=y_i)$| $\dfrac{10}{12}$  |  $\dfrac{2}{12}$  |  

!!! info "Loi de probabilité"

    La loi de probabilité d'une variable aléatoire $X$ est donnée par : 

    * l'ensemble des valeurs $\left\{x_1, x_2,..., x_r \right\}$ prises par la variable aléatoire
    * les probabilités $P(X=x_i)$ prises par $X$ pour toutes les valeurs $x_i$

    👉 Il est habituel de présenter une loi de probabilité sous la forme d'un tableau, comme vu dans l'exemple.

???+ question "Evenements incompatibles"

    **1.** Comment définit-on le fait que les évenements $A$ et $B$ sont incompatibles ?

    ??? success "Réponse"

        $A \cap B = \varnothing$

        Ce qui équivaut à : $P(A \cap B) = 0$

    **2.** Si $A$ et $B$ sont incompatibles que peut-on en déduire pour $P(A \cup B)$ ?

    ??? success "Réponse"

        $\boxed{P(A \cup B) = P(A) + P(B)}$ car $P(A \cup B) = P(A) + P(B) - P(A \cap B)$ avec $P(A \cap B) = 0$

    **3.** Dans l'exemple de l'introduction, les événements $X=0$ et $X=1$ sont-ils incompatibles ?

    ??? success "Réponse"

        On ne peut pas avoir à la fois $X=0$ et $X=1$ donc ces évenements sont bien incompatibles

    **4.** En déduire $P(X \leq 1 )$

    ??? success "Réponse"  

        $P(X \leq 1 ) = P(X=0)+P(X=1)=\dfrac{4}{12} + \dfrac{1}{12} = \dfrac{5}{12}$

    !!! info "Propriété"

        * Univers $\Omega= \left\{ x_1, x_2,..., x_r \right\}$
        * $X=x_1$, $X=x_2$,..., $X=x_r$ sont des évenements **incompatibles**

        On en déduit que : 

        !!! info "À retenir"

            $P(X=x_1)+P(X= x_2)+...+P(X=x_r)=1$

            On écrit aussi 

            $\sum_{i=1}^{r} P(X=x_i) = 1$

## III. Espérance mathématique

???+question "Jeu d'argent"

    On avait : 

    |$y_i$|-1|3|
    | :---    | :----: | :----: | 
    | $P(Y=y_i)$| $\dfrac{10}{12}$  |  $\dfrac{2}{12}$  |  

    Si on joue un très grand nombre de fois, quelle somme peut-on espérer gagner ?

    ??? success "Réponse"

        $-1 \times \dfrac{10}{12} + 3 \times \dfrac{2}{12}=-\dfrac{4}{12}=-\dfrac{1}{3}$

        En moyenne on perd environ 33 centimes.

        👉 On note $E(Y)=-\dfrac{1}{3}$


!!! info "Espérance mathématique"

    Soit $X$ une variable aléatoire, et l'univers $\Omega = \left\{ x_1, x_2,..., x_r \right\}$  
    On note $P(X=x_1)=p_i$

    L'espérance mathématique de la variable aléatoire $X$ se note $E(X)$

    $E(X)=p_1x_1+p_2x_2+...+p_rx_r=\sum_{i=1}^{r} p_i x_i$

!!! info "Espérance mathématique : interprétation"

    $E(X)$ est la moyenne des valeurs prises par $X$ lorsque l'expérience est répétée **un très grand nombre de fois**.

!!! info "Espérance et jeu"

    Lorsque $X$ représente le gain d'un jeu : 

    * Si $E(X)>0$ le jeu est **favorable** au joueur
    * Si $E(X)<0$ le jeu est **défavorable** au joueur
    * Si $E(X)=0$ le jeu est **équitable**


## IV. Exercices


[Exercices - série 1](https://coopmaths.fr/alea/?uuid=0f776&id=can1P08&uuid=e3c_2020_00_sujet45_2&uuid=e3c_2021_01_specimen2_3&uuid=e3c_2020_00_sujet29_2&uuid=e3c_2020_00_sujet63_3&uuid=e3c_2021_01_specimen3_3&uuid=e3c_2021_01_specimen4_3&uuid=e3c_2020_00_sujet65_4&uuid=e3c_2020_00_sujet64_4&v=eleve&es=0211001&title=){ .md-button target="_blank" rel="noopener" }

## V. Variance et écart type

!!! info "Variance"

    La variance est la moyenne des carrés des écarts à l'espérance.

!!! info "Formules pour la variance"

    Soit $X$ une variable aléatoire, et l'univers $\Omega = \left\{ x_1, x_2,..., x_r \right\}$  
    On note $P(X=x_i)=p_i$

    La variance de la variable aléatoire $X$ se note $V(X)$.
    Déduire de la définition la formule donnant $V(x)$. Vous utiliserez $E(x)$

    ??? tip "Astuce"

        Quel est le carré de l'écart entre $x_i$ et l'espérance $E(X)$ ?

        ??? success "Solution"

            $(x_i - E(X))^2$


    ??? success "Solution"

        $V(X)=p_1(x_1 - E(X))^2+p_2(x_2 - E(X))^2+...+p_r(x_r - E(x))^2=\sum_{i=1}^{r} p_i (x_i - E(X))^2$

!!! info "Variance - autre formule"

    On pourrait démontrer que la variance se calcule aussi avec la formule suivante :  

    $V(X)=E(X^2)-(E(X))^2$

!!! info "Écart type"

    L'écart type de la variable aléatoire $X$ se note $\sigma(X)$

    $\sigma(X)=\sqrt{V(X)}$

!!! info "Savoir utiliser votre calculatrice graphique"

    Il est **absolument nécessaire** de savoir utiliser votre calculatrice.  
    Etudier suivant le modèle de la votre : 

    * Calculatrice NUMWORKS : 

    [Calculatrice NUMWORKS](a_telecharger/calc_esperance_variance_ecart_type_numworks.pdf){ .md-button target="_blank" rel="noopener" }

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/tA9DCpLOhto?si=Q11AQ2pjxRQZTbnQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

    * Calculatrice CASIO : 

    [Calculatrice CASIO](a_telecharger/calc_esperance_variance_ecart_type_casio.pdf){ .md-button target="_blank" rel="noopener" }

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/xXUnzPVp5Zo?si=95tZbIp6v9fbunCE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

    * Calculatrice TI : 

    [Calculatrice TI](a_telecharger/calc_esperance_variance_ecart_type_ti83.pdf){ .md-button target="_blank" rel="noopener" }

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Z3rd-INoLtE?si=lHDPfFzYEgrEbHqu" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

???+ question "Exercice"

    [Exercice avec Variance et Ecart type](https://coopmaths.fr/alea/?uuid=e3c_2020_00_sujet43_4&v=eleve&es=0211001&title=){ .md-button target="_blank" rel="noopener" }














    














 
