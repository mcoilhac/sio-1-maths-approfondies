---
author: Mireille Coilhac
title: Découverte des suites arithmétiques
tags:
    - suites
---

???+ question "Exemple 1"

    Soit la suite $(u_n)$ définie par :  

    * $u_0=10$
    * Chaque terme est obtenu en ajoutant 5 au terme précédant.

    **1.** Déterminer $u_1$, $u_2$, $u_3$, $u_4$

    ??? success "Solution"

        $u_1=10+5=15, u_2=15+5=20, u_3=20+5=25, u_4=25+5=30$


    **2.** Compléter : 

    $u_{n+1} = u_n + ...$

    ??? success "Solution"

        $u_{n+1} = u_n + 5$

    **3.** Compléter : 

    * $u_1 = u_0 + ... \times 5$
    * $u_2 = u_0 + ... \times 5$
    * $u_3 = u_0 + ... \times 5$
    * $u_n = u_0 + ... \times 5$

    ??? success "Solution"

        * $u_1 = u_0 + 1 \times 5$
        * $u_2 = u_0 + 2 \times 5$
        * $u_3 = u_0 + 3 \times 5$
        * $u_n = u_0 + n \times 5$

    **4.** Déterminer si cette suite est croissante ou décroissante

    ??? tip "Coup de pouce"

        Il suffit de déterminer le signe de $u_{n+1} - u_n$

    ??? success "Solution"

        $u_{n+1} - u_n = 5$. Or $5>0$

        La suite $(u_n)$ est donc strictement croissante

    **5.** Calculer $u_0+u_1+u_2+u_3+u_4$ et le comparer à $5 \times \dfrac{u_0 + u_4}{2}$

    ??? success "Solution"

        $u_0+u_1+u_2+u_3+u_4 = 10+15+20+25+30=100$

        $5 \times \dfrac{u_0 + u_4}{2}= 5 \times \dfrac{10 + 30}{2}=100$

        On obtient le même résultat. C'est en fait un résultat général.


    !!! info "Suite arithmétique"

        On dit que la suite de l'exemple 1 est une suite arithmétique de premier terme $u_0=10$ et de raison $r=5$

    

???+ question "Exemple 2"

    Soit la suite $(u_n)$ définie pour tout entier naturel $n \geq 1$ par :  

    * $u_1=5$
    * Chaque terme est obtenu en ajoutant 10 au terme précédant.

    **1.** Déterminer $u_2$, $u_3$, $u_4$, $u_5$, 

    ??? success "Solution"

        $u_2=5+10=15, u_3=15+10=25, u_4=25+10=35, u_5=35+10=45$ 


    **2.** Compléter : 

    $u_{n+1} = u_n + ...$

    ??? success "Solution"

        $u_{n+1} = u_n + 10$

    **3.** Compléter : 

    * $u_2 = u_1 + ... \times ...$
    * $u_3 = u_1 + ... \times ...$
    * $u_4 = u_1 + ... \times ...$
    * $u_n = u_1 + ... \times ...$

    ??? success "Solution"

        * $u_2 = u_1 + 1 \times10$
        * $u_3 = u_1 + 2 \times 10$
        * $u_4 = u_1 + 3 \times 10$
        * $u_n = u_1 + (n-1) \times 10$

    **4.** Déterminer si cette suite est croissante ou décroissante

    ??? tip "Coup de pouce"

        Il suffit de déterminer le signe de $u_{n+1} - u_n$

    ??? success "Solution"

        $u_{n+1} - u_n = 10$. Or $10>0$

        La suite $(u_n)$ est donc strictement croissante

    **5.** Calculer $u_1+u_2+u_3+u_4$ et le comparer à $4 \times \dfrac{u_1 + u_4}{2}$

    ??? success "Solution"

        $u_1+u_2+u_3+u_4 = 5+15+25+35=80$

        $4 \times \dfrac{u_1 + u_4}{2}= 4 \times \dfrac{5 + 35}{2}=80$

        On obtient le même résultat. C'est en fait un résultat général.



    !!! info "Suite arithmétique"

        On dit que la suite de l'exemple 2 est une suite arithmétique de premier terme $u_1=5$ et de raison $r=10$

        
???+ question "Exemple 3"

    Soit la suite $(u_n)$ définie pour tout entier naturel $n \geq 0$.

    * $u_0=50$
    * Chaque terme est obtenu en soustrayant 10 au terme précédant.

    **1.** Déterminer $u_1$, $u_2$, $u_3$, $u_4$

    ??? success "Solution"

        $u_1=50-10=40, u_2=40-10=30, u_3=30-10=20, u_4=20-10=10$ 


    **2.** Compléter : 

    $u_{n+1} = u_n$  ...

    ??? success "Solution"

        $u_{n+1} = u_n -10$

    **3.** Compléter : 

    * $u_1 = u_0  - ... \times ...$
    * $u_2 = u_0  - ... \times ...$
    * $u_3 = u_0  - ... \times ...$
    * $u_4 = u_0  - ... \times ...$
    * $u_n = u_0  - ... \times ...$

    ??? success "Solution"

        * $u_1 = u_0 - 1 \times 10$
        * $u_2 = u_0 - 2 \times 10$
        * $u_3 = u_0 - 3 \times 10$
        * $u_4 = u_0 - 4 \times 10$
        * $u_n = u_0 - n \times 10$

    **4.** Déterminer si cette suite est croissante ou décroissante

    ??? tip "Coup de pouce"

        Il suffit de déterminer le signe de $u_{n+1} - u_n$

    ??? success "Solution"

        $u_{n+1} - u_n = -10$. Or $-10<0$

        La suite $(u_n)$ est donc strictement décroissante

    **5.** Calculer $u_0+u_1+u_2+u_3+u_4$ et le comparer à $5 \times \dfrac{u_0 + u_4}{2}$

    ??? success "Solution"

        $u_0+u_1+u_2+u_3+u_4 = 50+40+30+20+10=150$

        $5 \times \dfrac{u_0 + u_4}{2}= 5 \times \dfrac{50 + 10}{2}=150$

        On obtient le même résultat. C'est en fait un résultat général.


    !!! info "Suite arithmétique"

        On dit que la suite de l'exemple 3 est une suite arithmétique de premier terme $u_0=50$ et de raison $r=-10$

???+ question "Exemple 4"

    Soit la suite $(u_n)$ définie pour tout entier naturel $n \geq 0$.

    * On ne donne pas $u_0$, mais on donne $u_6$=15
    * Chaque terme est obtenu en additionnant 2 au terme précédant.

    **1.** Déterminer $u_7$, $u_8$, $u_9$

    ??? success "Solution"

        $u_7=15+2=17, u_8=17+2=19, u_9=19+2=21$

    **2.** Compléter :

    * $u_7 = u_6 + ... \times ...$
    * $u_8 = u_6 + ... \times ...$
    * $u_9 = u_6 + ... \times ...$
    * $u_n = u_6 + ... \times ...$

    ??? success "Solution"

        * $u_7 = u_6 + 1 \times 2$
        * $u_8 = u_6 + 2 \times 2$
        * $u_9 = u_6 + 3 \times 2$
        * $u_n = u_6 + (n-6) \times 2$


