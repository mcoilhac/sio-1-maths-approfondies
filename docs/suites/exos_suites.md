---
author: Mireille Coilhac
title: Exercices variés sur les suites
tags:
    - suites
---

## Exercice 1

???+ question "Exercice 1 : suites arithmétiques et géométriques"

    En 2018 Alice et Bob ont un salaire mensuel de 1200 €.

    * Chaque année, le salaire mensuel d'Alice augmente de 50 €. On note $u_n$ le salaire mensuel d'Alice en $2018+n$.
    * Chaque année, le salaire mensuel de Bob augmente de 3 %. On note $v_n$ le salaire mensuel de Bob en $2018+n$.

	**1.** Que vaut $u_0$ ? Quelle est la nature de la suite $(u_n)$ ?

    ??? success "Solution"

        On a $u_0=1200$ (salaire d'Alice en 2018).

    **2.** Exprimer $u_n$ en fonction de $n$.

    ??? success "Solution"

        Sachant que le salaire augmente de 50 € par an, on en déduit que $(u_n)$ est arithmétique de raison $r=50$ et de premier terme $u_0=1200$.
		
        On en déduit que $u_n = u_0 + n \times r = 1200 + n \times 50 =1200 + 50n$ pour tout entier $n$.  

    **3.** Calculer $u_{10}$ et interpréter le résultat.

    ??? success "Solution"

		On a donc  $u_{10} = u_0 + 10 \times r = 1200 + 10 \times 50 = 1700$.

    **4.** Que vaut $v_0$ ? Quelle est la nature de la suite $(v_n)$ ?

    ??? success "Solution"

        On a $v_0=1200$ (salaire de Bob en 2018)

    **5.** Exprimer $v_n$ en fonction de $n$.

    ??? success "Solution"

        Sachant que le salaire augmente de 3 % par an, ce qui correspond à un coefficient multiplicateur de $1 + \dfrac{3}{100}=1,03$, on en déduit que $(v_n)$ est géométrique de raison $q=1,03$ et de premier terme $v_0=1200$.

        on a donc $v_n = v_0 \times q^n = 1200 \times 1,03^n$ pour tout entier $n$.

    **6.** Déterminer le salaire mensuel de Bob en 2028.

    ??? success "Solution"

        Le salaire de Bob en 2018 est donné par $v_{10} = 1200 \times 1,03^{10} \approx 1612,70$, soit environ 1612,70 €.


    **7.** À partir de quelle année le salaire de Bob dépassera-t-il celui d'Alice ?

    ??? success "Solution"

        En utilisant une méthode de balayage sur la calculatrice, on obtient : 

        $$
        \left\{
        \begin{array}{ll}
            u_{22} = 2300 \\
            v_{22} \approx 2299,32\\
        \end{array}
        \right.
        $$

        et

        $$
        \left\{
        \begin{array}{ll}
            u_{23} = 2350 \\
            v_{23} \approx 2368,30\\
        \end{array}
        \right.
        $$

        C'est donc à partir de 2041 ($2018+23$) que le salaire de Bob dépassera celui d'Alice.



## Exercice 2

???+ question "Exercice 2 : suite auxiliaire"

    Soit $(u_n)$ la suite définie par $u_0=5$, et pour tout $n \in \mathbb{N}$, $u_{n+1} = 2 u_n +3$.

    **1.** On pose $v_n = u_n+3$. Montrer que la suite $(v_n)$ est géométrique.

    ??? success "Solution"

        On a $v_{n+1} = u_{n+1} + 3 = 2u_n+3+3=2u_n+6=2\left(u_n+3\right)=2v_n$ pour tout entier $n$.
	
	    On en déduit que $(v_n)$ est géométrique de raison $q=2$ et de premier terme $v_0 = u_0 + 3 = 5 + 3 = 8$.


    **2.** Exprimer $v_n$ en fonction de $n$.

    ??? success "Solution"

        On a donc $v_n = v_0 \times q^n = 8 \times 2^n$ pour tout entier $n$.

    **3.** En déduire la valeur de $u_n$ en fonction de $n$.

    ??? success "Solution"

        On en déduit donc $v_n = u_n + 3$  
        
        $u_n = v_n - 3 = 8 \times 2^n - 3$ pour tout entier $n$.

## Exercice 3

Faire les exercices du contrôle suivant donné en classe.
La correction des exercices se trouve à l fin du document.

[Contrôle sur les suites et correction](a_telecharger/DS_SIO_1_15_10_2024_def.pdf){ .md-button target="_blank" rel="noopener" }

## Crédits

D'après des exercices de Cédric Pierquet.