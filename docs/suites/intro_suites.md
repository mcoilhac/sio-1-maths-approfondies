---
author: Mireille Coilhac
title: Découverte des suites
tags:
    - suites
---


??? note "une suite numérique est ..."

    😂 Une suite de nombres

## I. Nom d'une suite et termes d'une suite

!!! example "Exemple 1"

    2, 2.5, 3, 3.5, 4, 4.5, 5 ... est une suite de nombres

    On peut dire que : 

    * le premier terme de cette suite est : 2
    * le deuxième terme de cette suite est : 2.5
    * ...
    * le sixième terme de cette suite est : 4.5

    On peut donc présenter cette suite dans un tableau comme ceci :

    $$
    \begin{array}{|l|c|c|c|c|c|c|c|c|c|}
    \hline
    \text{Rang n}  & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & ... \\
    \hline
    \text{Terme de rang n} & 2 & 2.5 & 3 & 3.5 & 4 & 4.5 & 5& 6 & ... \\
    \hline
    \end{array}
    $$

!!! info "Nommer une suite"

    Reprenons l'exemple 1 : 

    * On peut dire que c'est la suite $(u_n)$.
    * Le terme de rang 1 sera noté : $u_1$, celui de rang 2 sera noté $u_2$, ..., celui de rang 6 sera noté $u_6$

???+ question "À vous de jouer"

  
    En reprenant l'exemple 1, déterminer $u_1, u_2, u_3, u_7$

    ??? success "Solution"

        $u_1=2, u_2=2.5, u_3=3, u_7=5$

!!! example "Continuons avec l'exemple 1"

    2, 2.5, 3, 3.5, 4, 4.5, 5 ... est une suite de nombres

    On peut dire que : 

    * le premier terme de cette suite est : 2
    * le deuxième terme de ette suite est : 2.5
    * ...
    * le sixième terme de cette suite est : 4.5

    On peut donc présenter cette suite dans un tableau comme ceci :

    $$
    \begin{array}{|l|c|c|c|c|c|c|c|c|c|}
    \hline
    \text{Rang n}  & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & ... \\
    \hline
    \text{Terme de rang n} & 2 & 2.5 & 3 & 3.5 & 4 & 4.5 & 5& 6 & ... \\
    \hline
    \end{array}
    $$

!!! info "Nommer une suite"

    Reprenons l'exemple 1 : 

    * On peut dire que c'est la suite $(u_n)$.
    * Le terme de rang 1 sera noté : $u_1$, celui de rang 2 sera noté $u_2$, ..., celui de rang 6 sera noté $u_6$

???+ question "À vous de jouer"

  
    En reprenant l'exemple 1, déterminer $u_1, u_2, u_3, u_7$

    ??? success "Solution"

        $u_1=2, u_2=2.5, u_3=3, u_7=5$

!!! example "Exemple 2"

    1.1, 2.2, 3.3, 4.4,5.5, 6.6, 7.7 ... est une suite de nombres

    Dans la pratique, on considère souvent que le premier terme d'une suite est celui de rang 0.

    * le  terme de rang 0 de cette suite est : 1.1
    * le  terme de rang 1 de cette suite est : 2.2
    * ...
    * le  terme de rang 4 de cette suite est : 5.5

    On peut donc présenter cette suite dans un tableau comme ceci :

    $$
    \begin{array}{|l|c|c|c|c|c|c|c|c|c|}
    \hline
    \text{Rang n}  & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & ... \\
    \hline
    \text{Terme de rang n} &1.1 & 2.2 & 3.3 & 4.4 & 5.5 & 6.6 & 7.7 & 8.8 & ... \\
    \hline
    \end{array}
    $$

!!! info "Nommer une autre suite"

    Reprenons l'exemple 2 : 

    * On peut dire que c'est la suite $(v_n)$.
    * Le terme de rang 0 sera noté : $v_0$, celui de rang 1 sera noté $v_1$, ..., celui de rang 6 sera noté $v_6$

???+ question "À vous de jouer"

  
    En reprenant l'exemple 2, déterminer $v_0, v_1, v_2, v_3, v_7$

    ??? success "Solution"

        $v_0=1.1, v_1=2.2, v_2=3.3, v_3=4.4, v_7=8.8$

        
???+ question "Terme précédant ? Terme suivant ?"

    Comment se note le terme précédant $u_{85}$ ?

    ??? success "Solution"
        $u_{84}$

    Comment se note le terme suivant $v_{12}$ ?

    ??? success "Solution"
        $v_{13}$
  
    Comment se note le terme précédant $u_n$ ?

    ??? success "Solution"
        $u_{n-1}$

    Comment se note le terme suivant $v_n$ ?

    ??? success "Solution"
        $v_{n+1}$


    $v_{4+1}$ est-il égal à $v_4+1$ ?

    ??? success "Solution"

        $v_{4+1} \neq v_4+1$

        En effet $v_{4+1} = v_5 = 6.6$ et $v_4 + 1 = 5.5 + 1 = 6.5$

    D'une manière générale $v_{n+1}$ est-il toujours égal à $v_n+1$ ?

    ??? success "Solution"

        D'une manière générale $v_{n+1}$ est différent de $v_n+1$.

        En effet $v_{n+1}$ est le terme qui suit $v_{n}$, alors que $v_n+1$ est le terme $v_n$ auquel on a ajouté 1.

        !!! warning "Remarque"

            Il faut être bien minutieux dans votre écriture, pour que $v_{n+1}$ ne puisse pas être confondu avec $v_n+1$.


## II. Des suites données de façon explicite


???+ question "Exercice 1"

    On donne la suite $(u_n)$ définie pour tout entier $n\geq 0$ par $u_n=2n+5$.

    Donner les trois premiers termes de cette suite

    ??? success "Solution"
        $u_0=5, u_1=7, u_2=9$

    On donne la suite $(v_n)$ définie pour tout entier $n\geq 1$ par $v_n=2^n$.

    Donner les trois premiers termes de cette suite

    ??? success "Solution"
        $v_1=2^1=2, v_2=2^2=4, v_3=2^3=8$

    On donne la suite $(w_n)$ définie pour tout entier $n\geq 0$ par $w_n=2^n+10$.

    Donner les quatre premiers termes de cette suite

    ??? success "Solution"
        $w_0=2^0+10=1+10=11, w_1=2^1+10=2+10=12$   
        $w_2=2^2+10=4+10=14, w_3=2^3+10=8+10=18$

## III. Des suites données de proche en proche (par récurrence)      

???+ question "Exercice 2"

    On donne la suite $(u_n)$ définie par $u_0=5$ et pour tout entier naturel $n$ par 
    $u_{n+1}=2 \times u_n+10$

    **1.** Compléter les phrase suivante en utilisant "suivant" ou "précédant" : 

    Le terme de rang $n+1$ est égal à 2 fois le terme ... plus 10.

    Le terme ... le terme de rang $n$ est égal à 2 fois le terme de rang $n$ plus 10.

    ??? success "Solution"

        Le terme de rang $n+1$ est égal à 2 fois le terme **précédant** plus 10.

        Le terme **suivant** le terme de rang $n$ est égal à 2 fois le terme de rang $n$ plus 10.

    **2.** Donner les trois premiers termes de cette suite

    ??? success "Solution"

        $u_0=5, u_1=2 \times 5 + 10 = 20, u_2=2 \times 20 + 10 = 50$

    **3.** Déterminer $u_5$

    ??? tip "Astuce"

        Pour déterminer $u_5$, on doit connaître $u_4$, et pour déterminer $u_4$ on a besoin de $u_3$.

    ??? success "Solution"

        $u_3=2 \times u_2 + 10 = 2 \times 50 + 10 = 110$

        $u_4=2 \times u_3 + 10 = 2 \times 110 + 10 = 230$

        $u_5=2 \times u_4 + 10 = 2 \times 230 + 10 = 470$


!!! info "Suite définie par récurrence"

    On dit que la suite de l'exercice 2 est définie par récurrence.


## IV. Représentation de suites

!!! info "Représentation graphique"

    On peut représenter une suite sur un graphique en mettant les valeurs de $n$ en abscisse et les valeurs correspondantes de $u_n$ en ordonnée.

    !!! danger "Attention"

        Il ne faut absolument pas relier les points tracés entre eux. Les points n'existent que pour des abscisses entières, qui correspondent 
        aux rangs (numéros) des termes représentés


???+ question "À vous de jouer"

    On reprend la suite $(w_n)$ de l'exercice 1.  
    Représenter ses 4 premiers termes.

    ??? success "Solution"

        ![suite 1](images/suite1.png){ width=10% }

!!! info "Utiliser la calculatrice pour des suites définies de façon explicite"


    [Casio](a_telecharger/calc_suites_definies_explicitement_casio.pdf){ .md-button target="_blank" rel="noopener" }

    [Numworks](a_telecharger/calc_suites_definies_explicitement_numworks.pdf){ .md-button target="_blank" rel="noopener" }

    [Ti](a_telecharger/calc_suites_definies_explicitement_ti83.pdf){ .md-button target="_blank" rel="noopener" }


!!! info "Utiliser la calculatrice pour des suites définies par récurrencee"


    [Casio](a_telecharger/calc_suites_definies_par_recurrence_casio.pdf){ .md-button target="_blank" rel="noopener" }

    [Numworks](a_telecharger/calc_suites_definies_par_recurrence_numworks.pdf){ .md-button target="_blank" rel="noopener" }

    [Ti](a_telecharger/calc_suites_definies_par_recurrence_ti83.pdf){ .md-button target="_blank" rel="noopener" }


## V. Exercices

[Déterminer les termes d'une suite](https://coopmaths.fr/alea/?uuid=f0c2d&id=1AL10-3&n=5&d=10&alea=38H0&cd=1&uuid=b8c14&id=1AL10-4&n=5&d=10&alea=asLk&cd=1&v=eleve&es=0111001&title=){ .md-button target="_blank" rel="noopener" }

