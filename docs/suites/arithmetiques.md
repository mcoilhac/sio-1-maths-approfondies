---
author: Mireille Coilhac
title: Suites arithmétiques
tags:
    - suites
---

## I. Bilan sur les suites  arithmétiques

!!! info "Défintion par récurrence"

    On dit qu’une suite est **arithmétique** si chaque terme est obtenu à partir du précédent en ajoutant un **même** nombre $r$, appelé raison. Pour tout entier $n$, $u_{n+1} = u_n + r$ avec $u_0$ (ou $u_1$) donné.

!!! info "Propriétés"

    * Soit $u_0$ le premier terme et $r$ la raison d’une suite arithmétique, alors le terme de rang $n$ est **$u_n = u_0 + nr$**
 
    * Soit $u_1$ le premier terme et $r$ la raison d’une suite arithmétique alors le terme de rang $n$ est **$u_n = u_1 + (n-1)r$**

    * Cette expression permet de déterminer n’importe quel terme de la suite d’indice donné :   
    Si la suite $(u_n)$ est arithmétique de raison $r$ alors pour tout $n$ et $p$ entiers, on a **$u_n = u_p +(n-p)r$** 

!!! info "Visualiser"

    ![arithmetique rang 0](images/arithm_0.PNG){ width=30% }

    ![arithmetique rang 1](images/arithm_1.PNG){ width=30% }


!!! info "Méthode"

    Pour justifier qu'une suite est arithmétique, on montre que $u_{n+1}- u_n$ est un nombre constant $r$, indépendant de $n$, qui est appelé la raison. On peut aussi écrire une phrase qui décrive le fonctionnement de la suite.

!!! info "sens de variation"

    Soit $(u_n)$ une suite arithmétique de raison $r$. Alors :

	* Si $r<0$ alors la suite $(u_n)$ est strictement décroissante ;
    * Si $r=0$ alors la suite $(u_n)$ est constante ;
    * Si $r>0$ alors la suite $(u_n)$ est strictement croissante.

!!! info "Somme des premiers termes d'une suite arithmétique"

    La formule suivante est valable pour toute somme $S$ de termes consécutifs d'une suite arithmétique 

    $$S=\text{(nombre de termes de S)} \times \dfrac{\text{(premier terme de S)} + \text{(dernier terme de S)}}{2}.$$

    En particulier, si $(u_n)$  est de :

	* premier terme $u_0$, alors $S=u_0+u_1+\dots+u_n=(n+1) \times \dfrac{u_0+u_n}{2}$.
	* premier terme $u_1$, alors $S=u_1+u_2+\dots+u_n=n \times \dfrac{u_1+u_n}{2}$.

## II. Exercices

### Exercice 1

???+ question "Exercice 1"

	**1.** $(u_n)$ est une suite arithmétique de raison 3 et de premier terme $u_0=-11$. Calculer $u_{12}$.

    ??? success "Solution"

        Par définition des suites arithmétiques, on a $u_{12} = u_0 + 12 \times r = -11 + 12 \times 3 = 25$.

	**2.** $(v_n)$ est une suite arithmétique de raison $-7$ et de premier terme $v_1=12$. Calculer $v_{16}$.

    ??? success "Solution"

        Par définition des suites arithmétiques, on a $v_{16} = v_1 + 15 \times r = 12 + 15 \times (-7) = -93$.

	**3.** $(w_n)$ est une suite arithmétique telle que $w_0=12$ et $w_4=20$. Calculer $w_{20}$.

    ??? success "Solution"

        * $w_4 = w_0 + 4 \times r \Rightarrow 20 = 12 + 4 \times r \Rightarrow r = \dfrac{20-12}{4} = 2$ ;
		* $w_{20} = w_0 + 20 \times r = 12 + 20\times 2 = 52$.


	**4.** Déterminer le sens de variation de chacune des trois suites précédentes.

    ??? success "Solution"

        * pour $(u_n)$, on a $r=3 > 0$, donc $(u_n)$ est croissante ;
		* pour $(v_n)$, on a $r=-7 < 0$, donc $(v_n)$ est décroissante ;
		* pour $(w_n)$, on a $r=4 > 0$, donc $(w_n)$ est croissante ;


### Exercice 2

???+ question "Exercice 2"

    Soit $(u_n)$ une suite arithmétique de raison $r$.

    **1.** On donne $u_0  = 7$ et $r = 2$. Calculer $u_1$, $u_{25}$ et $u_{100}$.

    ??? success "Solution"

        * $u_1 = u_0 + r = 7 + 2 = 9$ ;
		* $u_{25} = u_0 + 25\times r = 7 + 25\times 2 = 57$ ;
		* $u_{100} = u_0 + 100\times r = 7 + 100\times 2 = 207$


	**2.** On donne $u_3  = 12$ et  $u_8= 0$. Calculer $r$, $u_0$  et $u_{18}$.

    ??? success "Solution"

        * $u_8 = u_3 + 5\times r \Rightarrow 0 = 12 + 5\times r \Rightarrow r = \dfrac{0-12}{5} = -2,4$ ;
		* $u_0 = u_3 - 3 \times r =12 - 3 \times (-2,4) = 19,2$ ;
		* $u_{18} = u_8 + 10 \times r = 0 + 10 \times (-2,4) = -24$.



### Exercice 3

???+ question "Exercice 3"

    Bob ouvre une pâtisserie. Le premier mois, il n'a que 5 clients réguliers. On note $u_1=5$.  
    Le nombre de clients réguliers augmente de 3 chaque mois. On note $u_n$ le nombre de clients réguliers le $n$-ième mois.

    **1.** Quelle est la nature de la suite $(u_n)$ ? Préciser sa raison et son terme initial.

    ??? success "Solution"

        À chaque étape, le nombre de clients augmente de 3, donc la suite $(u_n)$ est arithmétique de premier terme $u_1=5$ et de raison $r=3$.

    **2.** Exprimer $u_n$ en fonction de $n$.

    ??? success "Solution"

        On a donc $u_n = u_1 + (n-1) \times r = 5 + (n-1)\times3 = 5 + 3n-3 = 2+3n$ pour tout entier $n$.

	**3.** Au bout de combien de mois Bob aura-t-il plus de 100 clients réguliers ?

    ??? success "Solution"

        On cherche $n$ tel que $u_n > 100$  
        C'est-à dire $2+3n > 100$  
        $\Leftrightarrow 3n > 98$   
        $\Leftrightarrow  n > \dfrac{98}{3}$ 
        
        or $\dfrac{98}{3} \approx 32,67$.

        C'est donc au bout de 33 mois que Bob aura plus de 100 clients réguliers.

### Exercice 4

[Sommes de termes](https://coopmaths.fr/alea/?uuid=cfac9&id=1AL11-41&n=3&d=10&alea=aErC&cd=1&v=eleve&es=0111001&title=){ .md-button target="_blank" rel="noopener" }

### Exercice 5

???+ question "Exercice 5"

    **1.** $(u_n)$ est une suite arithmétique de raison $1,25$ et de terme initial $u_1 = 3,5$.
	
	Calculer $u_1 + u_2 + \dots + u_{50}$.

    ??? success "Solution"

        On a $u_1 + u_2 + \dots + u_{50} = \dfrac{(u_1 + u_{50}) \times 50}{2} = \dfrac{(3,5 + 3,5 + 49 \times 1,25)\times50}{2} = 1706,25$.

    **2.** $(u_n)$ est une suite arithmétique de raison $-3$ et de terme initial $u_0 = 40\,000$.
	
	Calculer $u_0 + u_1 + \dots + u_{200}$.

    ??? success "Solution"

        On a $u_0 + u_1 + \dots + u_{200} = \dfrac{(u_0 + u_{200}) \times 201}{2} = \dfrac{(40000 + 40000 + 200 \times (-3))\times201}{2} = 7979700$.


## Crédits

D'après les cours et exercices de Cédric Pierquet.


