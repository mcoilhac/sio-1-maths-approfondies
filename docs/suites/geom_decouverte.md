---
author: Mireille Coilhac
title: Découverte des suites géométriques
tags:
    - suites
---

???+ question "Exemple 1"

    Soit la suite $(u_n)$ définie par :  

    * $u_0=10$
    * Chaque terme est obtenu en multipliant par 5 le terme précédant.

    **1.** Déterminer $u_1$, $u_2$, $u_3$, $u_4$

    ??? success "Solution"

        $u_1=10 \times 5=50 , u_2=50 \times 5=250, u_3=250 \times 5 =1250, u_4=1250 \times 5=6250$


    **2.** Compléter : 

    $u_{n+1} = u_n \times ...$

    ??? success "Solution"

        $u_{n+1} = u_n \times 5$

    **3.** Compléter : 

    * $u_1 = u_0 \times 5^{...}$
    * $u_2 = u_0 \times 5^{...}$
    * $u_3 = u_0 \times 5^{...}$
    * $u_n = u_0 \times 5^{...}$

    ??? success "Solution"

        * $u_1 = u_0 \times 5^1$
        * $u_2 = u_0 \times 5^2$
        * $u_3 = u_0 \times 5^3$
        * $u_n = u_0 \times 5^n$

    **4.** Déterminer si cette suite est croissante ou décroissante

    ??? tip "Coup de pouce"

        Il suffit de déterminer le signe de $u_{n+1} - u_n$

    ??? success "Solution"

        $u_{n+1} - u_n = 10(5^{n+1} - 5^n)=10 \times 5^n(5-1)=10 \times 5^n \times 4$. On a donc pour tout entier $n$ : $u_{n+1} - u_n>0$

        La suite $(u_n)$ est donc strictement croissante

    **5.** Calculer $u_0+u_1+u_2+u_3+u_4$ et le comparer à $u_0 \times \dfrac{5^5-1}{5-1}$

    ??? success "Solution"

        $u_0+u_1+u_2+u_3+u_4 = 10+50+250+1250+6250=7810$

        $u_0 \times \dfrac{5^5-1}{5-1}= 10 \times \dfrac{3125-1}{5-1}=7810$

        On obtient le même résultat. C'est en fait un résultat général.


    !!! info "Suite géométrique"

        On dit que la suite de l'exemple 1 est une suite géométrique de premier terme $u_0=10$ et de raison $q=5$

    

???+ question "Exemple 2"

    Soit la suite $(u_n)$ définie pour tout entier naturel $n \geq 1$ par :  

    * $u_1=5$
    * Chaque terme est obtenu en multipliant par 10 le terme précédant.

    **1.** Déterminer $u_2$, $u_3$, $u_4$, $u_5$, 

    ??? success "Solution"

        $u_2=5 \times 10=50, u_3=50 \times 10 = 500, u_4=500 \times 10 = 5000, u_5=5000 \times 10 = 50000$ 


    **2.** Compléter : 

    $u_{n+1} = u_n \times ...$

    ??? success "Solution"

        $u_{n+1} = u_n \times 10$

    **3.** Compléter : 

    * $u_2 = u_1 \times 10 ^{...}$
    * $u_3 = u_1 \times 10 ^{...}$
    * $u_4 = u_1 \times 10 ^{...}$
    * $u_n = u_1 \times 10 ^{...}$

    ??? success "Solution"

        * $u_2 = u_1 \times 10 ^{1}$
        * $u_3 = u_1 \times 10 ^{2}$
        * $u_4 = u_1 \times 10 ^{3}$
        * $u_n = u_1 \times 10 ^{n-1}$

    **4.** Déterminer si cette suite est croissante ou décroissante

    ??? tip "Coup de pouce"

        Il suffit de déterminer le signe de $u_{n+1} - u_n$

    ??? success "Solution"

        $u_{n+1} - u_n = 5 \times  (10^n - 10^{n-1})= 5 \times 10^{n-1} (10-1)=5 \times 10^{n-1} \times 9$

        Pour tout entier $n$ on a $u_{n+1} - u_n >0$ donc la suite $(u_n)$ est donc strictement croissante

    **5.** Calculer $u_1+u_2+u_3+u_4$ et le comparer à $u_1 \times \dfrac{10^4-1}{10-1}$

    ??? success "Solution"

        $u_1+u_2+u_3+u_4 = 5+50+500+5000=5555$

         $u_1 \times \dfrac{10^4-1}{10-1}= 5 \times \dfrac{9999}{9}=5555$

        On obtient le même résultat. C'est en fait un résultat général.



    !!! info "Suite géométrique"

        On dit que la suite de l'exemple 2 est une suite géométrique de premier terme $u_1=5$ et de raison $q=10$

        

???+ question "Exemple 3"

    Soit la suite $(u_n)$ définie pour tout entier naturel $n \geq 0$.

    * On ne donne pas $u_0$, mais on donne $u_6$=30
    * Chaque terme est obtenu en multimpliant par 2 le terme précédant.

    **1.** Déterminer $u_7$, $u_8$, $u_9$

    ??? success "Solution"

        $u_7=30 \times 2=60, u_8=60 \times 2 = 120, u_9=120 \times 2 = 240$

    **2.** Compléter :

    * $u_7 = u_6 \times 2 ^{...}$
    * $u_8 = u_6 \times 2 ^{...}$
    * $u_9 = u_6 \times 2 ^{...}$
    * $u_n = u_6 \times 2 ^{...}$

    ??? success "Solution"

        * $u_7 = u_6 \times 2 ^{1}$
        * $u_8 = u_6 \times 2 ^{2}$
        * $u_9 = u_6 \times 2 ^{3}$
        * $u_n = u_6 \times 2 ^{n-6}$



???+ question "Sens de variation d'une suite géométrique"

    Suivre ce lien : [Représentation graphique de suites géométriques](https://www.geogebra.org/m/FCmbB4Bb){ .md-button target="_blank" rel="noopener" }

    Vous devez faire varier les deux curseurs $u_0$ et $q$, et regarder les différentes possibilités afin de remplir le tableau suivant :

    |   | $q<0$ | $0<q<1$ |$q=1$|$q>1$|
    | :----:   | :----:    |:----:   |:----:   |:----:   |
    |$u_0>0$   | ... | ... |... |... |
    | $u_0<0$ |... |... |... |... |

    ??? success "Solution"

        |   | $q<0$ | $0<q<1$ |$q=1$|$q>1$|
        | :----:   | :----:    |:----:   |:----:   |:----:   |
        |$u_0>0$   | ni croissante ni décroissante | décroissante |constante |croissante|
        | $u_0<0$ |ni croissante ni décroissante  |croissante |constante |décroissante |

