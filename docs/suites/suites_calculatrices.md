---
author: Mireille Coilhac
title: Suites et calculatrices
tags:
    - suites
---

## I. Usage de la calculatrice pour les suites définies de façon explicite

!!! info "Différentes calculatrices"

    [CASIO](a_telecharger/calc_suites_definies_explicitement_casio.pdf){ .md-button target="_blank" rel="noopener" }

    [Numworks](a_telecharger/calc_suites_definies_explicitement_numworks.pdf){ .md-button target="_blank" rel="noopener" }

    [Texas](a_telecharger/calc_suites_definies_explicitement_ti83.pdf){ .md-button target="_blank" rel="noopener" }


## II. Usage de la calculatrice pour les suites définies par récurrence

!!! info "Différentes calculatrices"

    [CASIO](a_telecharger/calc_suites_definies_par_recurrence_casio.pdf){ .md-button target="_blank" rel="noopener" }

    [Numworks](a_telecharger/calc_suites_definies_par_recurrence_numworks.pdf){ .md-button target="_blank" rel="noopener" }

    [Texas](a_telecharger/calc_suites_definies_par_recurrence_ti83.pdf){ .md-button target="_blank" rel="noopener" }

