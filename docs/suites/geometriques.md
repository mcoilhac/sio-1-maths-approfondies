---
author: Mireille Coilhac
title: Suites géométriques
tags:
    - suites
---

## I. Bilan sur les suites  géométriques

!!! info "Défintion par récurrence"

    On dit qu’une suite est **géométrique** si chaque terme est obtenu à partir du précédent en le multipliant par un **même** nombre $q$ **différent de zéro**, appelé raison. Pour tout entier $n$, $u_{n+1} = q \times u_n$ avec $u_0$ (ou $u_1$) donné.

!!! info "Propriétés"

    * Soit $u_0$ le premier terme et $q$ la raison d’une suite géométrique, alors le terme de rang $n$ est **$u_n = u_0 \times q^n$**
 
    * Soit $u_1$ le premier terme et $r$ la raison d’une suite géométrique alors le terme de rang $n$ est **$u_n = u_1 \times q^{n-1}$**

    * Cette expression permet de déterminer n’importe quel terme de la suite d’indice donné :   
    Si la suite $(u_n)$ est géométrique de raison $q$ alors pour tout $n$ et $p$ entiers, on a **$u_n = u_p \times q^{n-p}$** 

!!! info "Visualiser"

    ![géométrique rang 0](images/geom_0.PNG){ width=40% }

    ![géométrique rang 1](images/geom_1.PNG){ width=40% }


!!! info "Méthode"

    Pour justifier qu'une suite est géométrique, on montre que $u_{n+1} = q \times u_n$ , $q$ étant un nombre constant, indépendant de $n$, qui est appelé la raison. On peut aussi écrire une phrase qui décrive le fonctionnement de la suite.

!!! info "sens de variation"

    Soit $(u_n)$ une suite géométrique de raison $q$. Alors :

	* Si $q<0$ alors la suite $(u_n)$ n'est ni croissante ni décroissante car les termes se suivent en changeant de signe à chaque fois ;
    * Si $0<q<1$ et si le premier terme de la suite est positif $(u_n)$ est décroissante ;
    * Si $0<q<1$ et si le premier terme de la suite est négatif $(u_n)$ est croissante ;
    * Si $q>1$ et si le premier terme de la suite est positif $(u_n)$ est croissante ;
    * Si $q>1$ et si le premier terme de la suite est négatif $(u_n)$ est décroissante ;
    * Si $q=1$ alors la suite $(u_n)$ est constante.

    Pour visualiser les différents cas suivre ce lien et faire varier les curseurs $u_0$ et $q$: [Représentation graphique de suites géométriques](https://www.geogebra.org/m/FCmbB4Bb){ .md-button target="_blank" rel="noopener" }

    ??? note "Pour mémoriser sous forme de tableau"

        |   | $q<0$ | $0<q<1$ |$q=1$|$q>1$|
        | :----:   | :----:    |:----:   |:----:   |:----:   |
        |$u_0>0$   | ni croissante ni décroissante | décroissante |constante |croissante|
        | $u_0<0$ |ni croissante ni décroissante  |croissante |constante |décroissante |

!!! info "Somme des premiers termes d'une suite géométrique"

    La formule suivante est valable pour toute somme $S$ de termes consécutifs d'une suite géométrique de raison $q$
    
    $$S=\text{(premier terme de S)} \times \dfrac{1-q^{\text{nombre de termes de S}}}{1-q}.$$

    En particulier

	* $u_0+ u_1+\dots+u_n= u_0 \times \dfrac{1-q^{n+1}}{1-q}=u_0 \times \dfrac{q^{n+1}-1}{q-1}$
	* $u_1+\dots+u_n= u_1 \times \dfrac{1-q^{n}}{1-q}=u_1 \times \dfrac{q^{n}-1}{q-1}$

## II. Exercices

### Exercice 1

???+ question "Exercice 1"

	**1.** $(u_n)$ est une suite géométrique de raison 5 et de premier terme $u_0=0,44$. Calculer $u_{7}$.

    ??? success "Solution"

        Par définition des suites géométriques, on a $u_{7} = u_0 \times q^{7} = 0,44 \times 5^7 = 34375$.

	**2.** $(v_n)$ est une suite géométrique de raison $0,4$ et de premier terme $v_1=16$. Calculer $v_{6}$.

    ??? success "Solution"

        Par définition des suites géométriques, on a $v_{6} = v_1 \times q^{5} = 16 \times 0,4^5 = 0,16384$

	**3.** $(w_n)$ est une suite géométrique de raison strictement positive telle que $w_0=4,8$ et $w_2=19,2$. Calculer $w_{5}$.

    ??? success "Solution"

        * Déterminons tout d'abord la raison $q$ de cette suite: 
        $w_2 = w_0 \times q^2$   
        $19,2 = 4,8 \times q^2$   
        $q^2 = \dfrac{19,2}{4,8} = 4$   
        $q>0$ donc $q = \sqrt{4} = 2$  

        * On a donc $w_5 = w_0 \times q^5 = 4,8 \times 2^5 = 153,6$


	**4.** Déterminer le sens de variation de chacune des trois suites précédentes.

    ??? success "Solution"

        Le sens de variations d'une suite géométrique dépend du signe de son premier terme et de la comparaison de la raison par rapport à 1 :
		
        * pour $(u_n)$, on a $u_0 = 0,44$ et $q=5$, donc $u_0>0$ et $q>1$. $(u_n)$ est donc croissante ;
		* pour $(v_n)$, on a $v_1 = 16$ et $q=0,4$, donc $v_1>0$ et $0<q<1$. $(v_n)$ est donc décroissante ;
		* pour $(w_n)$, on a $w_0 = 4,8$ et $q=2$, donc $w_0>0$ et $q>1$. $(w_n)$ est donc croissante.



### Exercice 2

???+ question "Exercice 2"

    On considère la suite $(C_n)$ définie par $C_0=1200$ et $C_{n+1}=1,02 \times C_n$ pour tout entier $n$.

    **1.** Calculer $C_1$, $C_2$ et $C_3$

    ??? success "Solution"

        $C_1 = 1,02 \times C_0 = 1,02 \times 1200 = 1224$

        $C_2 = 1,02 \times C_1 = 1,02 \times 1224 = 1248,48$

        $C_3 = 1,02 \times C_2 = 1,02 \times 1248,48 = 1273,4496$.

    **2.** Quelle est la nature de la suite $(C_n)$ ? Préciser ses paramètres.

    ??? success "Solution"

        La formule de récurrence proposée est celle d'une suite géométrique, de premier terme $C_0=1200$ et de raison $q=1,02$.

    **3.** En déduire une expression de $C_n$ en fonction de $n$.

    ??? success "Solution"

        On en déduit que $C_n = C_0 \times q^n = 1200 \times 1,02^n$ pour tout entier $n$.

    **4.** Déterminer le sens de variation de $(C_n)$.

    ??? success "Solution"

        On sait que $C_0=1200$, et que $q=1,02$. Donc donc $C_0 >0$ et $q>1$. 
        
        On peut en déduire que $(C_n)$ est croissante.

    **5.** Déterminer le plus petit entier $n$ pour lequel on a $C_n \geq 6000$

    ??? success "Solution"

        Par balayage à la calculatrice on obtient : $u_{81} \approx 5967,54$ et $u_{82} \approx 6086,89$

        Le plus petit entier $n$ pour lequel on a $C_n \geq 6000$ est donc 82.

### Exercice 3

???+ question "Exercice 3"

    Le prix de vente à la mise sur le marché d'un modèle d’ordinateur est de 795 €. Chaque trimestre, le prix de vente 
    de ce modèle diminue de 10 %. Pour tout entier naturel $n$, $u_n$ désigne le prix de vente, exprimé en euro, de 
    ce modèle d’ordinateur $n$ trimestres après sa mise sur le marché.

    **1.** Donner $u_0$, puis calculer $u_1$ et $u_2$.

    ??? success "Solution"

        On a (d'après l'énoncé) $u_0 = 795$, et une diminution de 10 % correspond à un coefficient multiplicateur de $0,9$.

        $u_1 = u_0 \times 0,9 = 795 \times 0,9 = 715,5$ et $u_2 = u_1 \times 0,9 =715,5 \times 0,9 =643,95$.
        

    **2.** Déterminer la nature de la suite $(u_n)$ et préciser sa raison.

    ??? success "Solution"

        On peut donc en déduire que la suite $(u_n)$ est géométrique de raison $q=0,9$ et de premier terme $u_0=795$.

    **3.** Exprimer $u_n$ en fonction de $n$.

    ??? success "Solution"

        On a donc $u_n = u_0 \times q^n = 795 \times 0,9^n$ pour tout entier $n$.

    **4.** Calculer $u_5$.

    ??? success "Solution"

        On a $u_5 = u_0 \times q^5 = 795 \times 0,9^5 \approx 469,44$.

    **5.** À partir de combien de trimestres le prix de vente d'un tel ordinateur deviendra-t-il strictement inférieur à 300 € ?

    ??? success "Solution"

        Par balayage à la calculatrice on obtient : $u_9 \approx 308$ et $u_{10} \approx 277,2$

        Au bout de 10 trimestres le prix de vente sera strictement inférieur à 300 €.

### Exercice 4

???+ question "Exercice 4"

    Soit $(u_n)$ la suite géométrique de premier terme $u_0= 2$ et de raison $q =  \dfrac{5}{4}$.

    **1.** Calculer successivement les valeurs exactes de $u_1$ et $u_2$.

    ??? success "Solution"

        * $u_1 = u_0 \times \dfrac{5}{4} = 2 \times \dfrac{5}{4} = \dfrac{5}{2}$ ;
        * $u_2 = u_1 \times \dfrac{5}{4} = \dfrac{5}{2} \times \dfrac{5}{4} = \dfrac{25}{8}$ 


    **2.** Exprimer $u_n$ en fonction de $n$. Calculer la valeur exacte de $u_8$.

    ??? success "Solution"

        La suite $(u_n)$ étant géométrique, on en déduit que $u_n = u_0 \times q^n = 2 \times {\left(\dfrac54\right)}^n$ pour tout entier $n$.

        $u_8 = u_0 \times q^8 = 2 \times {\left(\dfrac54\right)}^8 = \dfrac{390625}{32768}$.

    **3.** Calculer la somme $u_0  + u_1+ u_2  + ...+ u_8$.

    ??? success "Solution"

        $u_0  + u_1+ u_2  + ...+ u_8 = u_0 \times \dfrac{1-q^{9}}{1-q} = 2 \times \dfrac{1-\left(\tfrac{5}{4}\right)^{9}}{1-\left(\tfrac{5}{4}\right)} \approx 51,60$.




### Exercice 5 


[Sommes de termes](https://coopmaths.fr/alea/?uuid=974a9&id=1AL11-42&n=4&d=10&alea=5RBt&cd=1&v=eleve&es=0111001&title=){ .md-button target="_blank" rel="noopener" }





## Crédits

D'après les cours et exercices de Cédric Pierquet.


