---
author: Votre nom
title: 🎁 Ressources
---

[Quelques bases très basiques](https://mcoilhac.forge.apps.education.fr/prerequis-maths-premiere/){ .md-button target="_blank" rel="noopener" }

[Utiliser les calculatrices graphiques pour les fonctions](https://mcoilhac.forge.apps.education.fr/prerequis-maths-premiere/calculatrice/calculatrices/){ .md-button target="_blank" rel="noopener" }

Les calculatrices **ne doivent pas** être en mode examen quand vous rentrez dans 
la salle d'examen. Vous devrez la mettre en examen lorsque le surveillant le dira.

[Mettre ou retirer le mode examen des calculatrices](https://math.univ-lyon1.fr/irem/spip.php?article895){ .md-button target="_blank" rel="noopener" }

[Mettre ou retirer le mode examen des calculatrices CASIO](casio_mode_examen.pdf){ .md-button target="_blank" rel="noopener" }

[Consignes mode examen](Consignes_calculatrice_mode_examen.pdf){ .md-button target="_blank" rel="noopener" }

