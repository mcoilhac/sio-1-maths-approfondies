---
author: Mireille Coilhac
title: Loi binomiale
tags:
    - probabilités
---

## I. Exemple et vocabulaire

???+ question "Pile ou Face"

    On dispose d’une pièce de monnaie truquée qui tombe sur Pile dans 70% des cas et qui tombe sur face dans 30% des cas. On appelle succès le fait d’obtenir pile.  
    
    Quelle est $p$ la probabilité de succès?

    ??? success "Solution"

        $p=\dfrac{7}{10}=0,7$

!!! info "Épreuve de Bernoulli"

    Une épreuve de Bernoulli est expérience simple avec **exactement deux résultats possibles** souvent appelés **succès** et **échec**.

???+ question "Pile ou Face quatre fois de suite"

    On lance quatre fois de suite la pièce de l'exemple. Soit X la variable aléatoire comptant le nombre de fois où on obtient Pile. Les quatre résultats sont indépendants. Si on obtient Pile, puis Face, puis Pile, puis Face, on notera la résultat obtenu PFPF.
    
    **1.** Représenter l'arbre donnant toutes les issues possibles, avec les probabilités indiquées.

    ??? success "Solution"

        ![chemins](images/chemins_piece.png){ width=40%}

    **2.** Rajouter au bout des branches les probabilités de chaque chemin, et la valeur de $X$

    ??? success "Solution"

        ![chemins et probas](images/arbre_probas.png){ width=60%}

    **3.** Donner la loi de probabilité de la variable aléatoire $X$

    ??? success "Solution"

        $$
        \begin{array}{|c|c|}
        \hline
        x_i  &  P(X = x_i)  \\
        \hline
        0 &  1 \times (0.7)^0 (0.3)^4  \\
        \hline
        1 &  4 \times (0.7)^1 (0.3)^3  \\
        \hline
        2 &  6 \times (0.7)^2 (0.3)^2  \\
        \hline
        3 &  4 \times (0.7)^3 (0.3)^1  \\
        \hline
        4 &  1 \times (0.7)^4 (0.3)^0  \\
        \hline
        \end{array}
        $$

???+ note "Le nombre de chemins"

    $P(X = 2)$ est égal à $6 \times (0.7)^2 (0.3)^2$ car en lisant l'arbre, on trouve $6$ fois la probabilité $(0.7)^2 (0.3)^2$
    Ce nombre $6$ correspond au nombre de chemins de l'arbre pour obtenir $2$ succès parmis $4$ essais (les 4 lancés successifs). 
    Il se note $\binom{4}{2}$ et vaut 6.

???+ question "Déterminer des nombres de chemins"

    En utilisant l'exemple ci-dessus, et l'arbre, donner les notations et résultats de : 
    
    * nombre de chemins de l'arbre pour obtenir $0$ succès parmis $4$ essais.
    * nombre de chemins de l'arbre pour obtenir $1$ succès parmis $4$ essais.
    * nombre de chemins de l'arbre pour obtenir $3$ succès parmis $4$ essais.
    * nombre de chemins de l'arbre pour obtenir $4$ succès parmis $4$ essais.

    ??? success "Solution"

        * nombre de chemins de l'arbre pour obtenir $0$ succès parmis $4$ essais : $\binom{4}{0}=1$
        * nombre de chemins de l'arbre pour obtenir $1$ succès parmis $4$ essais : $\binom{4}{1}=4$
        * nombre de chemins de l'arbre pour obtenir $3$ succès parmis $4$ essais : $\binom{4}{3}=4$
        * nombre de chemins de l'arbre pour obtenir $4$ succès parmis $4$ essais : $\binom{4}{4}=1$

!!! info "Coefficients binomiaux"

    $\binom{n}{k}$ est un coefficient binomial. Il se lit **$k$ parmi $n$**

    Il représente le nombre de chemins donnant $k$ succès parmi $n$ si on a répété $n$ fois de façons 
    indépendante une expérience de Bernoulli.

???+ question "Loi de probabilité et coefficients binomiaux"

    Donner la loi de probabilité de l'exemple de la pièce, en utilisant les coefficients binomiaux.

    ??? success "Solution"

        $$
        \begin{array}{|c|c|}
        \hline
        x_i &  P(X = x_i)  \\
        \hline
        0 & \binom{4}{0} (0.7)^0 (0.3)^4  \\
        \hline
        1 & \binom{4}{1} (0.7)^1 (0.3)^3  \\
        \hline
        2 & \binom{4}{2} (0.7)^2 (0.3)^2  \\
        \hline
        3 & \binom{4}{3} (0.7)^3 (0.3)^1  \\
        \hline
        4 & \binom{4}{4} (0.7)^4 (0.3)^0  \\
        \hline
        \end{array}
        $$

???+ question "Valeurs particulières des coefficients binomiaux"

    Soit $n$ un entier naturel. En vous aidant de l'arbre, déterminer : $\binom{n}{0}$, $\binom{n}{1}$, $\binom{n}{n-1}$, $\binom{n}{n}$

    ??? success "Solution"

        * $\binom{n}{0}=1$
        * $\binom{n}{1}=n$
        * $\binom{n}{n-1}=n$
        * $\binom{n}{n}=1$

!!! loi binomiale "$\mathcal{B}(n, p)$"

    La variable aléatoire $x$ étudiée dans ce paragraphe suit la loi binomiale de paramètres $n=4$ et $p=0,7$ notée $\mathcal{B}(4; 0,7)$



!!! info "Probabilité $P(X=k)$ pour $X$ suivant une loi binomiale $\mathcal{B}(n, p)$"

    ![formule](images/formule_binomiale.png){width=40%}

## II. Loi binomiale

!!! info "Utilisation de la loi binomiale (à connaître par cœur)"

    Lorsque l'on répète $n$ fois, dans des conditions identiques et indépendantes, une expérience aléatoire à deux issues (le succès, de probabilité $p$ et l'échec, de probabilité $1-p$) alors la variable aléatoire $X$ qui compte le nombre de succès suit la loi binomiale de paramètres $n$ et $p$

    On note $X$ suit $\mathcal{B}(n; p)$.

!!! info "Les tirages"

    Dans le cas de tirages avec remise, il y a **indépendance** des tirages puisque la probabilité du succès ne change pas d'un tirage à l'autre. En revanche, dans le cas de tirages sans remise, l'indépendance des tirages n'existe plus puisque la composition de l'urne change d'un tirage à l'autre.  Cependant,  lorsque le nombre d'objets tirés est "petit" par  rapport au nombre total d'objets dans l'urne, on peut considérer que les tirages sont indépendants.

!!! Info "Les coefficients binomiaux"

    Il existe ine formule mathématique pour calculer $\binom{n}{k}$ :

    $$
    \binom{n}{k} = \frac{n!}{k!(n-k)!}
    $$

???+ question "Quelques calculs de coefficients binomiaux"

    Calculer  $\binom{4}{0}$, $\binom{4}{1}$, $\binom{4}{2}$

    ??? success "Solution"

        *  $\binom{4}{0}=\dfrac{4!}{0! \times 4!}=\dfrac{4!}{ 1 \times 4!}=1$
        *  $\binom{4}{1}=\dfrac{4!}{1! \times 3!}=\dfrac{4 \times 3!}{ 1! \times 3!}=4$
        *  $\binom{4}{2}=\dfrac{4!}{2! \times 2!}=\dfrac{4 \times 3 \times 2!}{ 2! \times 2!}=\dfrac{12}{2}=6$


???+ question "Probabilité $P(X=k)$ pour $X$ suivant une loi binomiale $\mathcal{B}(n, p)$"

    $P(X=k)=...$

    ??? sucess "Formule"

        $P(X = k) = \binom{n}{k} p^k (1-p)^{n-k}$

## III. Espérance, variance et écart type dans le cas d'une loi binomiale.

!!! info "Propriétés"

    Soit $X$ une variable aléatoire suivant une loi binomiale $\mathcal{B}(n, p)$

    * $E(X) = np$
    * $V(X) = np(1-p)$
    * $\sigma = \sqrt{np(1-p)}$

## IV. Calculatrice

Il est **absolument nécessaire** de savoir utiliser votre calculatrice.  
    
Etudier suivant le modèle de la votre : 

* Calculatrice NUMWORKS : 

[Calculatrice NUMWORKS](a_telecharger/CALC-Loi-binomiale-Numworks.pdf){ .md-button target="_blank" rel="noopener" }

* Calculatrice CASIO : 

[Calculatrice CASIO](a_telecharger/CALC-Loi-binomiale-Casio.pdf){ .md-button target="_blank" rel="noopener" }

* Calculatrice TI : 

[Calculatrice TI](a_telecharger/CALC-Loi-binomiale-TI83.pdf){ .md-button target="_blank" rel="noopener" }

## V. Exercices

[Exercices - Loi binomiale](https://coopmaths.fr/alea/?uuid=AN2023Q5&id=TSP1-QCM02&alea=gKlx&uuid=bac_2022_05_sujet1_etrangers_4&alea=PcTP&uuid=bac_2021_06_etrangers_2&alea=JnWI&uuid=bac_2021_06_metropole_2&alea=Bbjq&uuid=bac_2021_09_metropole_6&alea=zHvI&uuid=bac_2022_05_sujet1_madagascar_1&alea=6W6a&uuid=bac_2021_05_ameriquenord_1&alea=1w63&uuid=bac_2021_06_polynesie_2&alea=uLil&uuid=bac_2022_10_sujet1_caledonie_4&alea=E8wy&uuid=bac_2024_09_sujet1_polynesie_1&alea=Fci1&v=eleve&es=1111001&title=){ .md-button target="_blank" rel="noopener" }









    























